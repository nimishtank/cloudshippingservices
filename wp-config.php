<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'i4314322_wp2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oDb]<*O+4?rHJS?1k||E15mvW,QnYlUH5oE=LwnAy:|||(#&D,i{H20Lo9`IM.iH');
define('SECURE_AUTH_KEY',  '??}Y Qu:/<bzy`SRTI)}Cn`pb+x_@5s2QS8tC(uHx)vptkZ=Q!pECOVjv8)3E 43');
define('LOGGED_IN_KEY',    '3fxNImByD~YaxBI/d4p-dbhWCs}#$Q#02XN@lgq5_,4.+fxN<[F L{TDx~76I]AK');
define('NONCE_KEY',        'ul;olw-Bl4Flo}/DaBX|a,,#KIcsH`keXN#6u)fdQSJX24aXi: (!zca?Y_/vi~t');
define('AUTH_SALT',        'd937VmK^xSnQVLbgC8Zj(8tbRb:^)`J#YxN3s0/4tP<i!i)v|:MF-+}U(]16$On+');
define('SECURE_AUTH_SALT', ')9+t^mC9Jwm<,TWFf:a;]<`jFQ-aiM,@00,dA=v  v)mHj19XN=WBU&EK.!{0~Y ');
define('LOGGED_IN_SALT',   ')/JwG{c*rQTCLIB0&m YGP,YLbl|)b,yq0J`dcnN6ULf$8?d)R2:z;uzB(d{K7]B');
define('NONCE_SALT',       'C^Asuow2xvesp]e%)KBl@%/}=5==iu)-ORx|S;$tmCsn4=-Ioc641;ARa=4!g_[O');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
