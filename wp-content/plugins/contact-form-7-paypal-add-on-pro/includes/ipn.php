<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


// used for testing to make sure the IPN can listen to URL calls.
add_action('template_redirect','cf7pp_ipn_test');
function cf7pp_ipn_test() {

	if (isset($_REQUEST['cf7pp_test'])) {
		echo "Contact Form 7 - PayPal Add-on Pro - Test Successful";
		exit;
	}
}


// paypal post
function cf7pp_listen_for_paypal_ipn() {

	if (isset($_REQUEST['mc_gross'])) {
		
		// Check the request method is POST
		if (isset( $_SERVER['REQUEST_METHOD'] ) && $_SERVER['REQUEST_METHOD'] != 'POST' ) {
			return;
		}
		
		// Set initial post data to empty string
		$post_data = '';
		
		// Fallback just in case post_max_size is lower than needed
		if ( ini_get( 'allow_url_fopen' ) ) {
			$post_data = file_get_contents( 'php://input' );
		} else {
			// If allow_url_fopen is not enabled, then make sure that post_max_size is large enough
			ini_set( 'post_max_size', '12M' );
		}
		// Start the encoded data collection with notification command
		$encoded_data = 'cmd=_notify-validate';
		
		// Get current arg separator
		$arg_separator = cf7pp_get_php_arg_separator();
		
		// Verify there is a post_data
		if ( $post_data || strlen( $post_data ) > 0 ) {
			// Append the data
			$encoded_data .= $arg_separator.$post_data;
		} else {
			// Check if POST is empty
			if ( empty( $_POST ) ) {
				// Nothing to do
				return;
			} else {
				// Loop through each POST
				foreach ( sanitize_text_field($_POST) as $key => $value ) {
					// Encode the value and append the data
					$encoded_data .= $arg_separator."$key=" . urlencode( $value );
				}
			}
		}
		
		// Convert collected post data to an array
		parse_str( $encoded_data, $encoded_data_array );
		
		foreach ( $encoded_data_array as $key => $value ) {
		
			if ( false !== strpos( $key, 'amp;' ) ) {
				$new_key = str_replace( '&amp;', '&', $key );
				$new_key = str_replace( 'amp;', '&' , $new_key );
				
				unset( $encoded_data_array[ $key ] );
				$encoded_data_array[ $new_key ] = $value;
			}
			
		}
		
		// Get the PayPal redirect uri
		$paypal_redirect = cf7pp_get_paypal_redirect( true );
		
		//disable paypal verification setting
		$options = get_option('cf7pp_options');
		
		// set default validation, in case the value is not set
		$validation = '1';
		
		// do not run validation - 0 for no validation
		if ($options['validation'] == '0') {
			$validation = '0';
		}
		
		if ($validation == '1') {
			
			
			// Validate the IPN
			
			$remote_post_vars      = array(
				'method'           => 'POST',
				'timeout'          => 45,
				'redirection'      => 5,
				'httpversion'      => '1.1',
				'blocking'         => true,
				'headers'          => array(
					'host'         => 'www.paypal.com',
					'connection'   => 'close',
					'content-type' => 'application/x-www-form-urlencoded',
					'post'         => '/cgi-bin/webscr HTTP/1.1',
					
				),
				'sslverify'        => false,
				'body'             => $encoded_data_array
			);
			
			// Get response
			$api_response = wp_remote_post( cf7pp_get_paypal_redirect(), $remote_post_vars );
			
			if ( is_wp_error($api_response)) {
				$error = json_encode($api_response);
				return; // Something went wrong
			}
			
			if ( $api_response['body'] !== 'VERIFIED' ) {
				$error = json_encode($api_response);
				return; // Response not okay
			}
			
		}
			
		// Check if $post_data_array has been populated
		if ( ! is_array( $encoded_data_array ) && !empty( $encoded_data_array ) ) {
			return;
		}
			
		$defaults = array(
			'txn_type'       => '',
			'payment_status' => ''
		);
		
		// Get POST values from PayPal
		$encoded_data_array = wp_parse_args( $encoded_data_array, $defaults );
		
		
		// log all post data
		//$encoded_data_arraya = serialize($encoded_data_array);
		
		
		
		
		if (isset($encoded_data_array['txn_type']) && ($encoded_data_array['txn_type'] == "cart")) {
			
			
			$post_id = sanitize_text_field($encoded_data_array['custom']);
			
			$content = get_post($post_id);
			
			if (!empty($content)) {
				
				// content
				$array = $content->post_content;
				$string_back = unserialize(base64_decode($array));
				
				if (is_array($string_back)) {
					
					$values = array_values ($string_back);
					
					
					// get paypal transaction id
					$txn_id = $encoded_data_array['txn_id'];
					
					
					
					
					// file attachments
					$upload_dir = wp_upload_dir();
					$basedir = $upload_dir['basedir'];
					$uploaddir = "/cf7pp_uploads";
					
					// mail 1 attachments
					if (isset($values[0]['attachments'])) {
						$mail1_attachments = $values[0]['attachments'];
						$mail1_attachments = explode("\r\n",$mail1_attachments);
						$mail1_attachments = array_filter($mail1_attachments);
					}
					
					// mail 2 attachments
					if (isset($values[0]['attachments2'])) {
						$mail2_attachments = $values[0]['attachments2'];
						$mail2_attachments = explode("\r\n",$mail2_attachments);
						$mail2_attachments = array_filter($mail2_attachments);
					}
					
					
					// files will be auto deleted after 1 day by /includes/admin/files.php
					
					
					
					// mail 1
					$mail1 = $values[0];
					$mail1 = str_replace("[txn_id]", $txn_id, $mail1);
					$result = WPCF7_Mail::send($mail1,'mail');
					
					
					
					// mail 2
					if (isset($values[1])) {
						$mail2 = $values[1];
						$mail2 = str_replace("[txn_id]", $txn_id, $mail2);
						$result = WPCF7_Mail::send($mail2,'mail');
					}
					
					
					// hook - pre delete cf7pp post
					do_action('cf7pp_payment_successful_pre_post_delete', $post_id, $txn_id);
					
					
					wp_delete_post($post_id,true);
					
					// hook - payment successful
					do_action('cf7pp_payment_successful', $post_id, $txn_id);
					
				}
				
			}
		}
	}
}
add_action('template_redirect','cf7pp_listen_for_paypal_ipn');




function cf7pp_get_paypal_redirect( $ssl_check = false ) {

	// Check if SSL is being used on the site
	if ( is_ssl() || ! $ssl_check ) {
		$protocal = 'https://';
	} else {
		$protocal = 'http://';
	}

	// Check the current payment mode
	$options = get_option('cf7pp_options');
	
	if ($options['mode'] == "1") {
		$paypal_uri = $protocal . 'www.sandbox.paypal.com/cgi-bin/webscr';
	} else {
		$paypal_uri = $protocal . 'www.paypal.com/cgi-bin/webscr';
	}
	

	return apply_filters( 'cf7pp_paypal_uri', $paypal_uri );
}




// get php arg separator
function cf7pp_get_php_arg_separator() {
	return ini_get('arg_separator.output');
}