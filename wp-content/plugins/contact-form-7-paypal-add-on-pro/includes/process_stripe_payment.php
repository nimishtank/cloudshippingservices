<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


// listen for and process stripe charge
add_action('wp_ajax_cf7pp_stripe_charge', 'cf7pp_stripe_charge');
add_action('wp_ajax_nopriv_cf7pp_stripe_charge', 'cf7pp_stripe_charge');

function cf7pp_stripe_charge($request) {

	// make sure using POST
	if ( isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] != 'POST' && isset($_POST['stripeToken']) ) {
		return;
	}

	// verify nonce
	$nonce = sanitize_text_field($_POST['nonce']);
	$salt = wp_salt();
	if (!wp_verify_nonce($nonce,'cf7pp_'.$salt)) { die( "<span style='color: red;'>".__('Error - Security validation failed.','cf7pp')."</span>" ); }


	
	
	// get settings page options
	$options = get_option('cf7pp_options');
	
	
	// get variables
	$token = 						sanitize_text_field($_POST['token']['id']);

	// form id
	$id = 							sanitize_text_field($_POST['id']);
	



	// get tags_id which is the post id that holds the converated tag values, delete this post when done
	
	$tags_id = sanitize_text_field($_SESSION['tags_id']);
	
	$content = get_post($tags_id);
	
	// exit if already run
	if (empty($content)) {
		exit;
	}
	
	$array = $content->post_content;
	$tags_back = unserialize(base64_decode($array));
	foreach ($tags_back as $k => $v ) { $tags[$k] = $v; }
	
	// exit if not tags found
	if (!isset($tags) || empty($tags)) {
		exit;
	}
	
	
	
	
	
	
	
	
	// get tags
	$stripe_key_sec = 	$tags['sec_key'];
	$currency = 		$tags['currency'];
	
	
	
	
	
	
	
	
	// currency
	if (empty($currency)) { 		$currency = "USD"; }
	
	
	
	//// static Values
	
	// desc
	$desc = 						get_post_meta($id, "_cf7pp_name", true);
	
	
	
	
	//// dynamic values
	$metadata = array(
		'ID/SKU'			=> $tags['id'],
		'shipping' 			=> cf7pp_format_currency($tags['shipping']),
		'tax' 				=> cf7pp_format_currency($tags['tax']),
		'tax_rate' 			=> $tags['tax_rate'],
	);
	
	
	
	
	
	
	// items
	if (empty($tags['name'])) 		{						$tags['name'] =  "(No item name)"; }
	if (empty($tags['name2'])) 		{ 						$tags['name2'] = "(No item name)"; }
	if (empty($tags['name3'])) 		{ 						$tags['name3'] = "(No item name)"; }
	if (empty($tags['name4'])) 		{ 						$tags['name4'] = "(No item name)"; }
	if (empty($tags['name5'])) 		{ 						$tags['name5'] = "(No item name)"; }
	
	
	$i = "1";

	if (!empty($tags['price'])) {
		if ($tags['quantity'] != "0") {
			
			if (empty($tags['quantity'])) 	{ 					$tags['quantity']  = "1"; }
			$tags['price'] = 									cf7pp_format_currency($tags['price']);
			$metadata[$i.'_amount'] = 							$tags['price'];
			$metadata[$i.'_quantity'] = 						$tags['quantity'];
			$metadata[$i.'_item_name'] = 						$tags['name'];
			$metadata[$i.'_option_a_name'] = 					$tags['text_menu_a_name'].' - '.$tags['text_menu_a'];
			$metadata[$i.'_option_b_name'] = 					$tags['text_menu_b_name'].' - '.$tags['text_menu_b'];
			$i++;
		}
	}

	if (!empty($tags['price2'])) {
		if ($tags['quantity2'] != "0") {
			
			if (empty($tags['quantity2'])) 	{ 					$tags['quantity2']  = "1"; }
			$tags['price2'] = 									cf7pp_format_currency($tags['price2']);
			$metadata[$i.'_amount'] = 							$tags['price2'];
			$metadata[$i.'_quantity'] = 						$tags['quantity2'];
			$metadata[$i.'_item_name'] = 						$tags['name2'];
			$metadata[$i.'_option_a_name'] = 					$tags['text_menu_a_name2'].' - '.$tags['text_menu_a2'];
			$metadata[$i.'_option_b_name'] = 					$tags['text_menu_b_name2'].' - '.$tags['text_menu_b2'];
			$i++;
		}
	}

	if (!empty($tags['price3'])) {
		if ($tags['quantity3'] != "0") {
			
			if (empty($tags['quantity3'])) 	{ 					$tags['quantity3']  = "1"; }
			$tags['price3'] = 									cf7pp_format_currency($tags['price3']);
			$metadata[$i.'_amount'] = 							$tags['price3'];
			$metadata[$i.'_quantity'] = 						$tags['quantity3'];
			$metadata[$i.'_item_name'] = 						$tags['name3'];
			$metadata[$i.'_option_a_name'] = 					$tags['text_menu_a_name3'].' - '.$tags['text_menu_a3'];
			$metadata[$i.'_option_b_name'] = 					$tags['text_menu_b_name3'].' - '.$tags['text_menu_b3'];
			$i++;
		}
	}

	if (!empty($tags['price4'])) {
		if ($tags['quantity4'] != "0") {
			
			if (empty($tags['quantity4'])) 	{ 					$tags['quantity4']  = "1"; }
			$tags['price4'] = 									cf7pp_format_currency($tags['price4']);
			$metadata[$i.'_amount'] = 							$tags['price4'];
			$metadata[$i.'_quantity'] = 						$tags['quantity4'];
			$metadata[$i.'_item_name'] = 						$tags['name4'];
			$metadata[$i.'_option_a_name'] = 					$tags['text_menu_a_name4'].' - '.$tags['text_menu_a4'];
			$metadata[$i.'_option_b_name'] = 					$tags['text_menu_b_name4'].' - '.$tags['text_menu_b4'];
			$i++;
		}
	}

	if (!empty($tags['price5'])) {
		if ($tags['quantity5'] != "0") {
			
			if (empty($tags['quantity5'])) 	{ 					$tags['quantity5']  = "1"; }
			$tags['price5'] = 									cf7pp_format_currency($tags['price5']);
			$metadata[$i.'_amount'] = 							$tags['price5'];
			$metadata[$i.'_quantity'] = 						$tags['quantity5'];
			$metadata[$i.'_item_name'] = 						$tags['name5'];
			$metadata[$i.'_option_a_name'] = 					$tags['text_menu_a_name5'].' - '.$tags['text_menu_a5'];
			$metadata[$i.'_option_b_name'] = 					$tags['text_menu_b_name5'].' - '.$tags['text_menu_b5'];
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




	// convert amount to cents
	$amount = $tags['amount_total'] * 100;

	// default status
	$status = '';

	// class
	\Stripe\Stripe::setApiKey($stripe_key_sec);

	// Create a charge: this will charge the user's card
	try {

		$charge = \Stripe\Charge::create(array(
			"amount" 				=> $amount, // Amount in cents
			"currency" 				=> $currency,
			"source" 				=> $token,
			"description" 			=> $desc,
			"metadata" 				=> $metadata,
		));
		
		
		// for debugging
		//error_log($charge);
		
		
		// result variables
		$txn_id = 				sanitize_text_field($charge->id);
		
		$status = 'completed';
		
		
		
		
		
		
		wp_delete_post($tags_id,true);
		
		
		
		
		
		
		
		
		
		
		
		
		$content = get_post($_SESSION['input_id']);
		
		if (!empty($content)) {
			
			// content
			$array = $content->post_content;
			$string_back = unserialize(base64_decode($array));
			$values = array_values ($string_back);
			
			
			// file attachments
			$upload_dir = wp_upload_dir();
			$basedir = $upload_dir['basedir'];
			$uploaddir = "/cf7pp_uploads";
			
			// mail 1 attachments
			if (isset($values[0]['attachments'])) {
				$mail1_attachments = $values[0]['attachments'];
				$mail1_attachments = explode("\r\n",$mail1_attachments);
				$mail1_attachments = array_filter($mail1_attachments);
			}
			
			// mail 2 attachments
			if (isset($values[0]['attachments2'])) {
				$mail2_attachments = $values[0]['attachments2'];
				$mail2_attachments = explode("\r\n",$mail2_attachments);
				$mail2_attachments = array_filter($mail2_attachments);
			}
			
			
			// files will be auto deleted after 1 day by /includes/admin/files.php
			
			
			
			// mail 1
			$mail1 = $values[0];
			$mail1 = str_replace("[txn_id]", $txn_id, $mail1);
			$result = WPCF7_Mail::send($mail1,'mail');
			
			
			
			// mail 2
			if (isset($values[1])) {
				$mail2 = $values[1];
				$mail2 = str_replace("[txn_id]", $txn_id, $mail2);
				$result = WPCF7_Mail::send($mail2,'mail');
			}
			
			
			// hook - pre delete cf7pp post
			do_action('cf7pp_payment_successful_pre_post_delete', $tags['new_post_id'], $txn_id);
			
			
			wp_delete_post($tags['new_post_id'],true);
			
			// hook - payment successful
			do_action('cf7pp_payment_successful', $tags['new_post_id'], $txn_id);
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	} catch(\Stripe\Error\Card $e) {
		// Since it's a decline, \Stripe\Error\Card will be caught

		$body = $e->getJsonBody();
		$err  = $body['error'];
		$reason = $err['message'];

	} catch (\Stripe\Error\RateLimit $e) {
		// Too many requests made to the API too quickly

		$body = $e->getJsonBody();
		$err  = $body['error'];
		$reason = $err['message'];


	} catch (\Stripe\Error\InvalidRequest $e) {
		// Invalid parameters were supplied to Stripe's API

		$body = $e->getJsonBody();
		$err  = $body['error'];
		$reason = $err['message'];


	} catch (\Stripe\Error\Authentication $e) {
		// Authentication with Stripe's API failed
		// (maybe you changed API keys recently)

		$body = $e->getJsonBody();
		$err  = $body['error'];
		$reason = $err['message'];


	} catch (\Stripe\Error\ApiConnection $e) {
		// Network communication with Stripe failed

		$body = $e->getJsonBody();
		$err  = $body['error'];
		$reason = $err['message'];


	} catch (\Stripe\Error\Base $e) {
		// Display a very generic error to the user, and maybe send
		// yourself an email

		$body = $e->getJsonBody();
		$err  = $body['error'];
		$reason = $err['message'];


	} catch (Exception $e) {
		// Something else happened, completely unrelated to Stripe

		$body = $e->getJsonBody();
		$err  = $body['error'];
		$reason = $err['message'];

	}


	// transaction failed
	if ($status != 'completed') {
		$status = 'failed';
		$txn_id = '';
	}

	$html_success = "
		<table>
		<tr><td>".$options['status'].": </td><td>".$options['success']."</td></tr>
		<tr><td>".$options['order']." #: </td><td>$txn_id</td></tr>
		</table>
	";

	// response array
	$response = array(
		'response'		 =>		$status,
		'html_success' =>		$html_success,
	);


	echo json_encode($response);

	wp_die();

}
