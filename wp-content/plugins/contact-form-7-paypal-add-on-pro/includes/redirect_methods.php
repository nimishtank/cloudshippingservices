<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


// returns the form id of the forms that have paypal enabled - used for redirect method 1 and method 2
function cf7pp_forms_enabled() {

	// array that will contain which forms paypal is enabled on
	$enabled = array();
	
	$args = array(
		'posts_per_page'   => 999,
		'post_type'        => 'wpcf7_contact_form',
		'post_status'      => 'publish',
	);
	$posts_array = get_posts($args);
	
	
	// loop through them and find out which ones have paypal enabled
	foreach($posts_array as $post) {
		
		$post_id = $post->ID;
		
		// paypal
		$enable = get_post_meta( $post_id, "_cf7pp_enable", true);
		
		if ($enable == "1") {
			$enabled[] = $post_id.'|paypal';
		}
		
		// stripe
		$enable_stripe = get_post_meta( $post_id, "_cf7pp_enable_stripe", true);
		
		if ($enable_stripe == "1") {
			$enabled[] = $post_id.'|stripe';
		}
		
	}

	return json_encode($enabled);

}



// hook into contact form 7 - after send - for redirect method 1 and 2 - for paypal only
add_action('template_redirect','cf7pp_redirect_method');
function cf7pp_redirect_method() {

	if (isset($_GET['cf7pp_redirect'])) {
		
		// get the id from the cf7pp_before_send_mail function theme redirect
		$post_id = $_GET['cf7pp_redirect'];
		
		// redirect
		cf7pp_redirect($post_id);
		exit;
		
	}
}


// for redirect method 2 - this must be loaded for redirect method 2 regardless of if the form has paypal enabled
$options = get_option('cf7pp_options');

if ($options['redirect'] == "2"  || $options['redirect'] == '') {

	if (!defined('WPCF7_LOAD_JS')) {
		define('WPCF7_LOAD_JS', false);
	}

}


// hook into contact form 7 - before send for redirect method 1 and method 2
add_action('wpcf7_before_send_mail', 'cf7pp_before_send_mail');
function cf7pp_before_send_mail() {

	global $current_user;
	global $cf7;

	$wpcf7 = WPCF7_ContactForm::get_current();

	// need to save submission for later and the variables get lost in the cf7 javascript redirect
	$submission_orig = WPCF7_Submission::get_instance();

	if ($submission_orig) {
		// get form post id
		$posted_data = $submission_orig->get_posted_data();
		$post_id = $posted_data['_wpcf7'];
		$gateway = strtolower(get_post_meta($post_id, "_cf7pp_gateway", true));
		
		
		$enable = 			get_post_meta( $post_id, "_cf7pp_enable", true);
		$enable_stripe = 	get_post_meta( $post_id, "_cf7pp_enable_stripe", true);
		
		if ($enable == '1' || $enable_stripe == '1') {
			
			
			
			
			
			
			// begin new code
			
			
			
			
			
			
			
			
			$email = get_post_meta($post_id, "_cf7pp_email", true);
			
			// file upload handling
			
			// get files
			$find = 		"wpcf7_uploads";
			$uploaddir = 	"/cf7pp_uploads";
			$upload_dir = 	wp_upload_dir();
			$basedir = 		$upload_dir['basedir'];
			$array = 		$submission_orig;
			$test = 		serialize($array);
			$length = 		strlen($find);
			$count = 		substr_count($test,$find);
			$files_arr = 	(array) null;
			$files_order = 	(array) null;
			
			function get_string_between($string, $start, $end) {
				$string = " ".$string;
				$ini = strpos($string,$start);
				if ($ini == 0) return "";
				$ini += strlen($start);
				$len = strpos($string,$end,$ini) - $ini;
				return substr($string,$ini,$len);
			}
			
			// finding file item names and order
			for ($i=0; $i<$count; $i++) {
				$result = get_string_between($test,'uploaded_files', ';}');
				$files_order = explode('"',$result);
			}
			
			$files_order = array_filter($files_order);
			function myFilter($string) { return strpos($string, ':') === false;	}
			$files_order = array_filter($files_order, 'myFilter');
			function myFiltera($string) { return strpos($string, '/') === false; }
			$files_order = array_filter($files_order, 'myFiltera');
			$files_order = array_values($files_order);
			
			// finding file paths
			for ($i=0; $i<$count; $i++) {
				$result = $basedir."/".$find.$parsed = get_string_between($test,$find, '"');
				$test = substr($test, strpos($test,$find) + $length);
				$parsed_folder = explode("/",$parsed);
				mkdir($basedir.$uploaddir."/".$parsed_folder['1'], 0777, true);
				copy($result,$basedir.$uploaddir.$parsed);
				array_push($files_arr,$parsed);
			}
			
			
			include_once ('mail_tags.php');
			
			// skip if price is 0, if setting is enabled
			$skip = get_post_meta( $post_id, "_cf7pp_skip", true);
			
			$skip_code = get_post_meta( $post_id, "_cf7pp_skip_code", true);
			
			
			
			// check if skip code contains mutiple form elements
			$skip_code = explode(',',$skip_code);
			$array_skip_result = is_array($skip_code);
			
			$skip_value = '';
			
			if ($array_skip_result) {
				
				foreach ($skip_code as $code) {
					
					// only contains one form element
					if (isset($posted_data[$code])) {
						
						$a_result = is_array($posted_data[$code]);
						
						if ($a_result) {
							$skip_value = (int)$skip_value + (int)$posted_data[$code][0];
						} else {
							$skip_value = (int)$skip_value + (int)$posted_data[$code];
						}
						
					} else {
						$skip_value = '';
					}
					
				}
				
			} else {
				
				// only contains one form element
				if (isset($posted_data[$skip_code])) {
					$skip_value = $posted_data[$skip_code];
				} else {
					$skip_value = '';
				}
				
			}
			
			
			// check if $skip_value is an array - this can happen for radio buttons, but would not happen for dropdowns
			$array_result = is_array($skip_value);
			if ($array_result) {
				$skip_value = $skip_value[0];
			}
			
			
			if ($skip == "1" && $tags['price'] == "0" || $skip_value == "0") {
				$no_redirect = "1";
				wp_delete_post($tags_id,true);
			} else {
				$no_redirect = "0";
			}		
			
			//if ($enable == "1" && $no_redirect != "1") {
			//	if ($options['redirect'] == "1") {
					//$site_url = get_site_url();
					//$path = $site_url.'/?cf7pp_redirect='.$new_post_id.'&orig='.$post_id.'&tags='.$tags_id;
					//$wpcf7->set_properties(array('additional_settings' => "on_sent_ok: \"location.replace('".$path."');\"",));
			//	}
			//}
			
			// do not send the email depending on settings
			if  ($email == "1" || $email == "3") {
				
				
				// old method used to skip sending mail - this worked in cf7 < 4.9
				$wpcf7->skip_mail = true;
				
				
				// method used to skip redirect in cf7 4.9+
				
				
				// define the wpcf7_skip_mail callback 
				function cf7pp_filter_wpcf7_skip_mail($skip_mail,$contact_form) {
					
					$skip_mail = true;
					
					return $skip_mail;
				}; 
				
				// add the skip mail filter 
				add_filter('wpcf7_skip_mail','cf7pp_filter_wpcf7_skip_mail',10,2);
				
			}
			
			
			
			// if $no_redirect is 1, then skip redirect
			
			// end new code
			
			
			// set defaults in case uplugin has been updated without savings the settings page
			if (!isset($tags['pub_key'])) {
				$tags['pub_key'] 			= '';
				$tags['stripe_state'] 		= 'live';
				$options['default_symbol'] 	= '$';
			}
			
			
			$gateway_orig = $gateway;
			
			if ($enable == '1') {
				$gateway = 'paypal';
			}
			
			if ($enable_stripe == '1') {
				$gateway = 'stripe';			
			}
			
			if ($enable == '1' && $enable_stripe == '1') {
				$gateway = $posted_data[$gateway_orig];
			}
			
			
			$_SESSION['gateway'] = 			$gateway;
			$_SESSION['skip'] = 			$no_redirect;
			$_SESSION['tags_id'] = 			$tags_id;
			$_SESSION['input_id'] = 		$new_post_id; // post id of saved email
			$_SESSION['pub_key'] = 			$tags['pub_key'];
			$_SESSION['stripe_state'] = 	$tags['stripe_state'];
			$_SESSION['amount_total'] = 	$tags['amount_total'];
			$_SESSION['default_symbol'] = 	$options['default_symbol'];
			
		}
	}
}






// after submit post for js - used for method 1 and 2 for paypal and stripe
add_action('wp_ajax_cf7pp_get_form_post', 'cf7pp_get_form_post_callback');
add_action('wp_ajax_nopriv_cf7pp_get_form_post', 'cf7pp_get_form_post_callback');
function cf7pp_get_form_post_callback() {

	$response = array(
		'gateway'         		=> $_SESSION['gateway'],
		'skip'         			=> $_SESSION['skip'],
		'pub_key'         		=> $_SESSION['pub_key'],
		'amount_total'         	=> $_SESSION['amount_total'],
		'default_symbol'        => $_SESSION['default_symbol'],
	);

	echo json_encode($response);

	wp_die();
}


// return stripe payment form
add_action('wp_ajax_cf7pp_get_form_stripe', 'cf7pp_get_form_stripe_callback');
add_action('wp_ajax_nopriv_cf7pp_get_form_stripe', 'cf7pp_get_form_stripe_callback');
function cf7pp_get_form_stripe_callback() {

	// generate nonce
	$salt = wp_salt();
	$nonce = wp_create_nonce('cf7pp_'.$salt);

	$options = get_option('cf7pp_options');
	
	
	$result = '';
	
	if ( (empty($options['pub_key_test']) && $options['mode_stripe'] == '1') || (empty($options['pub_key_live']) && $options['mode_stripe'] == '2') ) {
		$result .= 'Stripe Error. Admin: Please enter your Stripe Keys on the settings page.';
		
		$response = array(
			'html'         		=> $result,
		);
		
		echo json_encode($response);
		wp_die();
	}
	
	
	// show stripe test mode div
	if ($_SESSION['stripe_state'] == 'test') {
		$result .= "<a href='https://stripe.com/docs/testing#cards' target='_blank' class='test-mode'>test mode</a>";
	}
	
	// strip payment form
	$result .= "<div class='cf7pp_stripe'>";
		$result .= "<form method='post' id='cf7pp-payment-form'>";
			$result .= "<div class='cf7pp_body'>";
				$result .= "<div class='cf7pp_row'>";
					$result .= "<div class='cf7pp_details_input'>";
						$result .= "<label for='cf7pp_stripe_credit_card_number'>"; $result .= __($options['card_number'],'cf7pp_stripe'); $result .= "</label>";
						$result .= "<div id='cf7pp_stripe_credit_card_number'></div>";
					$result .= "</div>";
						
					$result .= "<div class='cf7pp_details_input'>";
						$result .= "<label for='cf7pp_stripe_credit_card_csv'>"; $result .= __($options['card_code'],'cf7pp_stripe'); $result .= "</label>";
						$result .= "<div id='cf7pp_stripe_credit_card_csv'></div>";
					$result .= "</div>";
				$result .= "</div>";
				$result .= "<div class='cf7pp_row'>";
					$result .= "<div class='cf7pp_details_input'>";
						$result .= "<label for='cf7pp_stripe_credit_card_expiration'>"; $result .= __($options['expiration'],'cf7pp_stripe'); $result .= "</label>";
						$result .= "<div id='cf7pp_stripe_credit_card_expiration''></div>";
					$result .= "</div>";
						
					$result .= "<div class='cf7pp_details_input'>";
						$result .= "<label for='cf7pp_stripe_credit_card_zip'>"; $result .= __($options['zip'],'cf7pp_stripe'); $result .= "</label>";
						$result .= "<div id='cf7pp_stripe_credit_card_zip'></div>";
					$result .= "</div>";
				$result .= "</div>";
				$result .= "<div id='card-errors' role='alert'></div>";
			$result .= "<br />&nbsp;<input id='stripe-submit' value='".$options['pay']." ".$options['default_symbol'].$_SESSION['amount_total']."' type='submit'>";
			$result .= "<div>";
		$result .= "<input type='hidden' id='cf7pp_stripe_nonce' value='$nonce'>";
		$result .= "</form>";
	$result .= "<div>";


	$response = array(
		'html'         		=> $result,
	);

	echo json_encode($response);
	wp_die();
}
