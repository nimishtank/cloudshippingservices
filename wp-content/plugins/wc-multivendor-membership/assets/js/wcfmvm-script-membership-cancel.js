jQuery(document).ready( function($) {
	// Membership Cancel
	$('#wcfm_membership_cancel_button').click(function( event ) {
		event.preventDefault();
		if( $('#wcfm_profile_form').length > 0 ) {
			$('#wcfm_profile_form').block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
		} else {
			$('#wcfm_vendor_manage_form_membership_expander').block({
				message: null,
				overlayCSS: {
					background: '#fff',
					opacity: 0.6
				}
			});
		}
		var data = {
			action       : 'wcfmvm_membership_cancel',
			memberid     : $('#wcfm_membership_cancel_button').data('memberid'),
			membershipid : $('#wcfm_membership_cancel_button').data('membershipid')
		}	
		$.post(wcfm_params.ajax_url, data, function(response) {
			if(response) {
				$response_json = $.parseJSON(response);
				if($response_json.status) {
					if( $('#wcfm_profile_form').length > 0 ) {
						$('#wcfm_profile_form .wcfm-message').html('<span class="wcicon-status-completed"></span>' + $response_json.message).addClass('wcfm-success').slideDown( "slow", function() {
							if( $response_json.redirect ) window.location = $response_json.redirect;	
						} );	
					} else {
						$('#wcfm_vendor_manage_form_membership_expander .wcfm-message').html('<span class="wcicon-status-completed"></span>' + $response_json.message).addClass('wcfm-success').slideDown( "slow", function() {
							window.location = window.location.href;
						} );
					}
				} else {
					$('.wcfm-message').html('').removeClass('wcfm-success').slideUp();
					if( $('#wcfm_profile_form').length > 0 ) {
						$('#wcfm_profile_form .wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + $response_json.message).addClass('wcfm-error').slideDown();
					} else {
						$('#wcfm_vendor_manage_form_membership_expander .wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + $response_json.message).addClass('wcfm-error').slideDown();
					}
				}
				$('#wcfm_profile_form').unblock();
				$('#wcfm_vendor_manage_form_membership_expander').unblock();
			}
		});
	});
	
	// Vendor Membership change by Admin
	$('#wcfm_modify_vendor_membership').click(function( event ) {
		event.preventDefault();
		$('.wcfm-message').html('').removeClass('wcfm-success').slideUp();
		if( !$('#wcfm_change_vendor_membership').val() ) return false;
		$('#wcfm_vendor_manage_form_membership_expander').block({
			message: null,
			overlayCSS: {
				background: '#fff',
				opacity: 0.6
			}
		});
		var data = {
			action       : 'wcfmvm_membership_change',
			memberid     : $('#wcfm_modify_vendor_membership').data('memberid'),
			membershipid : $('#wcfm_change_vendor_membership').val()
		}	
		$.post(wcfm_params.ajax_url, data, function(response) {
			if(response) {
				$response_json = $.parseJSON(response);
				if($response_json.status) {
					$('#wcfm_vendor_manage_form_membership_expander .wcfm-message').html('<span class="wcicon-status-completed"></span>' + $response_json.message).addClass('wcfm-success').slideDown( "slow", function() {
						window.location = window.location.href;
					} );	
				} else {
					$('#wcfm_vendor_manage_form_membership_expander .wcfm-message').html('<span class="wcicon-status-cancelled"></span>' + $response_json.message).addClass('wcfm-error').slideDown();
				}
				$('#wcfm_vendor_manage_form_membership_expander').unblock();
			}
		});
	});
});