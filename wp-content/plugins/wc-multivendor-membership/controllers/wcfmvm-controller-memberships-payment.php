<?php
/**
 * WCFM plugin controllers
 *
 * Plugin Memberships Payment Controller
 *
 * @author 		WC Lovers
 * @package 	wcfmvm/controllers
 * @version   1.0.0
 */

class WCFMvm_Memberships_Payment_Controller {
	
	public function __construct() {
		global $WCFM, $WCFMu;
		
		$this->processing();
	}
	
	public function processing() {
		global $WCFM, $WCFMvm, $wpdb, $wcfm_membership_payment_form_data;
		
		$wcfm_membership_payment_form_data = array();
	  parse_str($_POST['wcfm_membership_payment_form'], $wcfm_membership_payment_form_data);
	  
	  $wcfm_membership_payment_messages = get_wcfmvm_membership_payment_messages();
	  $has_error = false;
	  
	  if(isset($wcfm_membership_payment_form_data['member_id']) && !empty($wcfm_membership_payment_form_data['member_id'])) {
			$member_id = absint( $wcfm_membership_payment_form_data['member_id'] );
			$member_user = new WP_User(absint($member_id));
			$wcfm_membership = get_user_meta( $member_id, 'temp_wcfm_membership', true );
			$shop_name = get_user_meta( $member_id, 'store_name', true );
			$paymode = $_POST['paymode'];
			
			if( $wcfm_membership ) {
				update_user_meta( $member_id, 'wcfm_membership_paymode', $paymode );
				$required_approval = get_post_meta( $wcfm_membership, 'required_approval', true ) ? get_post_meta( $wcfm_membership, 'required_approval', true ) : 'no';
				
				if( ( $paymode == 'free' ) && ( $required_approval == 'no' ) ) {
					$has_error = $WCFMvm->register_vendor( $member_id );
					$WCFMvm->store_subscription_data( $member_id, $paymode, '', 'free_subscription', 'Completed', '' );
				} else {
					define( 'DOING_WCFM_EMAIL', true );
					
					// Vendor Approval Admin Email Notification
					$onapproval_admin_notication_subject = get_option( 'wcfm_membership_onapproval_admin_notication_subject', '{site_name}: A vendor application waiting for approval' );
					$onapproval_admin_notication_content = get_option( 'wcfm_membership_onapproval_admin_notication_content', '' );
					if( !$onapproval_admin_notication_content ) {
						$onapproval_admin_notication_content = "Dear Admin,
																										<br /><br />
																										A new vendor <b>{vendor_name}</b> (Store: <b>{vendor_store}</b>) has just applied to our membership plan <b>{membership_plan}</b>. 
																										<br /><br />
																										Kindly follow the below the link to approve/reject the application.
																										<br /><br />
																										Application: {notification_url} 
																										<br /><br />
																										Thank You";
					}
																	 
					$subject = str_replace( '{site_name}', get_bloginfo( 'name' ), $onapproval_admin_notication_subject );
					$message = str_replace( '{dashboard_url}', get_wcfm_url(), $onapproval_admin_notication_content );
					$message = str_replace( '{vendor_name}', $member_user->first_name, $message );
					$message = str_replace( '{vendor_store}', $shop_name, $message );
					$message = str_replace( '{membership_plan}', get_the_title( $wcfm_membership ), $message );
					$message = str_replace( '{notification_url}', get_wcfm_messages_url(), $message );
					$message = apply_filters( 'wcfm_email_content_wrapper', $message, __( 'Vendor Approval', 'wc-multivendor-membership' ) );
					
					wp_mail( get_bloginfo( 'admin_email' ), $subject, $message ); 
					
					// Vendor Approval Admin Desktop Notification
					$author_id = $member_id;
					$author_is_admin = 0;
					$author_is_vendor = 1;
					$message_to = 0;
					$wcfm_messages = sprintf( __( '<b>%s</b> (Store: <b>%s</b>) subscription for <b>%s</b> plan waiting for approval.', 'wc-multivendor-membership' ), $member_user->first_name, $shop_name, get_the_title( $wcfm_membership ) );
					$WCFM->frontend->wcfm_send_direct_message( $author_id, $message_to, $author_is_admin, $author_is_vendor, $wcfm_messages, 'vendor_approval' );
					
					update_user_meta( $member_id, 'wcfm_subscription_status', 'pending' );
				}
			
				// Reset Membership Session
				if( isset( $_SESSION['wcfm_membership'] ) && isset( $_SESSION['wcfm_membership']['membership'] ) && $_SESSION['wcfm_membership']['membership'] ) {
					unset( $_SESSION['wcfm_membership'] );
				}
			
				if(!$has_error) { echo '{"status": true, "message": "' . $wcfm_membership_payment_messages['subscription_success'] . '", "redirect": "' . add_query_arg( 'vmstep', 'thankyou', get_wcfm_membership_url() ) . '"}'; }
				else { echo '{"status": false, "message": "' . $wcfm_membership_payment_messages['subscription_failed'] . '"}'; }
			} else {
				echo '{"status": false, "message": "' . $wcfm_membership_payment_messages['no_memberid'] . '"}';
			}
	  } else {
			echo '{"status": false, "message": "' . $wcfm_membership_payment_messages['no_memberid'] . '"}';
		}
		
		die;
	}
}