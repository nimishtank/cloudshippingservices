<?php
/**
 * WCFM plugin core
 *
 * Plugin shortcode
 *
 * @author 		WC Lovers
 * @package 	wcfmvm/core
 * @version   1.0.0
 */
 
class WCFMvm_Shortcode {

	public $list_product;

	public function __construct() {
		// WC Frontend Manager Membership Shortcode
		add_shortcode('wcfm_vendor_membership', array(&$this, 'wcfm_vendor_membership'));
		
		// WC Frontend Manager Registration Shortcode
		add_shortcode('wcfm_vendor_registration', array(&$this, 'wcfm_vendor_registraion'));
		
		// WC Frontend Manager Subscribe Button Shortcode
		add_shortcode('wcfmvm_subscribe', array(&$this, 'wcfmvm_subscribe'));
	}

	public function wcfm_vendor_membership($attr) {
		global $WCFM;
		$this->load_class('vendor-membership');
		return $this->shortcode_wrapper(array('WCFM_Vendor_Membership_Shortcode', 'output'));
	}
	
	public function wcfm_vendor_registraion($attr) {
		global $WCFM;
		$this->load_class('vendor-registration');
		return $this->shortcode_wrapper(array('WCFM_Vendor_Registration_Shortcode', 'output'));
	}
	
	public function wcfmvm_subscribe( $attr ) {
		global $WCFM, $WCFMvm;
		
		if( is_admin() ) return;
		
		if( !wcfm_is_allowed_membership() ) return;
		
		$membership_id = 0;
		if ( isset( $attr['id'] ) && !empty( $attr['id'] ) ) { $membership_id = $attr['id']; }
		if( !$membership_id ) return;
		
		?>
		<input class="wcfm_membership_subscribe_button wcfm_submit_button button" type="button" data-membership="<?php echo $membership_id; ?>" value="<?php _e( "Subscribe Now", 'wc-multivendor-membership' ); ?>">
		
		<?php
	}
	
	/**
	 * Shortcode Wrapper
	 *
	 * @access public
	 * @param mixed $function
	 * @param array $atts (default: array())
	 * @return string
	 */
	public function shortcode_wrapper($function, $atts = array()) {
		ob_start();
		call_user_func($function, $atts);
		return ob_get_clean();
	}

	/**
	 * Shortcode CLass Loader
	 *
	 * @access public
	 * @param mixed $class_name
	 * @return void
	 */
	public function load_class($class_name = '') {
		global $WCFMvm;
		if ('' != $class_name && '' != $WCFMvm->token) {
			require_once ( $WCFMvm->plugin_path . 'includes/shortcodes/class-' . esc_attr($WCFMvm->token) . '-shortcode-' . esc_attr($class_name) . '.php' );
		}
	}

}
?>