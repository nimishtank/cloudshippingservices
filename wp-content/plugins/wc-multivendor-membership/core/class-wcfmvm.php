<?php

/**
 * WCFM Analytics plugin
 *
 * WCFM Analytics Core
 *
 * @author 		WC Lovers
 * @package 	wcfmvm/core
 * @version   1.0.0
 */

class WCFMvm {

	public $plugin_base_name;
	public $plugin_url;
	public $plugin_path;
	public $version;
	public $token;
	public $text_domain;
	public $library;
	public $template;
	public $shortcode;
	public $admin;
	public $frontend;
	public $ajax;
	private $file;
	public $settings;
	public $WCFMvm_fields;
	public $is_marketplace;
	public $WCFMvm_marketplace;
	public $WCFMvm_capability;
	public $wcfmvm_non_ajax;

	public function __construct($file) {

		$this->file = $file;
		$this->plugin_base_name = plugin_basename( $file );
		$this->plugin_url = trailingslashit(plugins_url('', $plugin = $file));
		$this->plugin_path = trailingslashit(dirname($file));
		$this->token = WCFMvm_TOKEN;
		$this->text_domain = WCFMvm_TEXT_DOMAIN;
		$this->version = WCFMvm_VERSION;
		
		// Installer Hook
		add_action( 'init', array( &$this, 'run_wcfmvm_installer' ) );
		
		add_action('init', array(&$this, 'init'));
		
	}
	
	/**
	 * initilize plugin on WP init
	 */
	function init() {
		global $WCFM, $WCFMvm;
		
		if( !session_id() ) session_start();
		
		// Register Vendor Membership Post Type
		register_post_type( 'wcfm_memberships', array( 'public' => false ) );
		
		// Init Text Domain
		$this->load_plugin_textdomain();
		
		if( ( version_compare( WC_VERSION, '3.0', '<' ) ) ) {
			//add_action( 'admin_notices', 'wcfm_woocommerce_version_notice' );
			return;
		}
		
		// Capability Controller
		if (!is_admin() || defined('DOING_AJAX')) {
			$this->load_class( 'capability' );
			$this->wcfmvm_capability = new WCFMvm_Capability();
		}
		
		// Check Marketplace
		$this->is_marketplace = wcfm_is_marketplace();

		// Init library
		$this->load_class('library');
		$this->library = new WCFMvm_Library();

		// Init ajax
		if( defined('DOING_AJAX') ) {
			$this->load_class('ajax');
			$this->ajax = new WCFMvm_Ajax();
		}

		if (!is_admin() || defined('DOING_AJAX')) {
			$this->load_class('frontend');
			$this->frontend = new WCFMvm_Frontend();
		}
		
		if( !defined('DOING_AJAX') ) {
			$this->load_class( 'non-ajax' );
			$this->wcfmvm_non_ajax = new WCFMvm_Non_Ajax();
		}
		
		// template loader
		$this->load_class( 'template' );
		$this->template = new WCFMvm_Template();
		
		// init shortcode
		$this->load_class( 'shortcode' );
		$this->shortcode = new WCFMvm_Shortcode();
		
		$this->wcfmvm_fields = $WCFM->wcfm_fields;
		
		// IPN listener
    $this->wcfmvm_ipn_listener();
    
    // WC Checkout for WCfM Membership products registration process
    add_action( 'woocommerce_order_status_completed', array( &$this, 'wcfmvm_registration_process_on_order_completed' ), 10, 1 );
    
    // ON WP delete user clean membership
    add_action( 'delete_user', array( &$this, 'wcfmvm_delete_user' ) );
	}
	
	/**
	 * Load Localisation files.
	 *
	 * Note: the first-loaded translation file overrides any following ones if the same translation is present
	 *
	 * @access public
	 * @return void
	 */
	public function load_plugin_textdomain() {
		$locale = function_exists( 'get_user_locale' ) ? get_user_locale() : get_locale();
		$locale = apply_filters( 'plugin_locale', $locale, 'wc-multivendor-membership' );

		load_textdomain( 'wc-multivendor-membership', WP_LANG_DIR . "/wc-frontend-manager-vendor-membership/wc-multivendor-membership-$locale.mo");
		load_textdomain( 'wc-multivendor-membership', ABSPATH . "wp-content/languages/plugins/wc-multivendor-membership-$locale.mo");
		load_textdomain( 'wc-multivendor-membership', $this->plugin_path . "/lang/wc-multivendor-membership-$locale.mo");
	}

	public function load_class($class_name = '') {
		if ('' != $class_name && '' != $this->token) {
			require_once ('class-' . esc_attr($this->token) . '-' . esc_attr($class_name) . '.php');
		} // End If Statement
	}

	// End load_class()

	/**
	 * Install upon activation.
	 *
	 * @access public
	 * @return void
	 */
	static function activate_wcfmvm() {
		global $WCFM, $WCFMvm, $wp_roles;
		
		require_once ( $WCFMvm->plugin_path . 'helpers/class-wcfmvm-install.php' );
		$WCFMvm_Install = new WCFMvm_Install();
		
		update_option('wcfmvm_installed', 1);
	}
	
	/**
	 * Check Installer upon load.
	 *
	 * @access public
	 * @return void
	 */
	function run_wcfmvm_installer() {
		global $WCFM, $WCFMvm, $wp_roles;
		
		if ( !get_option("wcfmvm_page_install") || !get_option("wcfmvm_installed") ) {
			require_once ( $WCFMvm->plugin_path . 'helpers/class-wcfmvm-install.php' );
			$WCFMvm_Install = new WCFMvm_Install();
			
			update_option('wcfmvm_installed', 1);
		}
	}

	/**
	 * UnInstall upon deactivation.
	 *
	 * @access public
	 * @return void
	 */
	static function deactivate_wcfmvm() {
		global $WCFM, $WCFMvm;
		
		delete_option('wcfmvm_installed');
	}
	
	function wcfmvm_membership_color_setting_options() {
		global $WCFM;
		
		$color_options = apply_filters( 'wcfmvm_membership_color_setting_options', array( 
																																				 'wcfmvm_field_base_highlight_color' => array( 'label' => __( 'Progress Bar Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_progress_bar_color_settings', 'default' => '#2a3344', 'element' => '.wcfm-membership-wrapper .wc-progress-steps li.done, .wcfm-membership-wrapper .wc-progress-steps li.active', 'style' => 'color', 'element2' => '.wcfm-membership-wrapper .wc-progress-steps li.done, .wcfm-membership-wrapper .wc-progress-steps li.active, .wcfm-membership-wrapper .wc-progress-steps li.done::before, .wcfm-membership-wrapper .wc-progress-steps li.active::before', 'style2' => 'border-color', 'element3' => '.wcfm-membership-wrapper .wc-progress-steps li.done::before', 'style3' => 'background' ),
																																				 'wcfmvm_field_table_border_color' => array( 'label' => __( 'Table Border Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_border_color_settings', 'default' => '#cccccc', 'element' => '#wcfm-main-contentainer .wcfm_membership_box_head, #wcfm-main-contentainer .wcfm_membership_box_body, #wcfm-main-contentainer .wcfm_membership_box_foot', 'style' => 'border-color' ),
																																				 'wcfmvm_field_table_text_color' => array( 'label' => __( 'Table Text Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_text_color_settings', 'default' => '#111111', 'element' => '#wcfm-main-contentainer .wcfm_membership_boxes', 'style' => 'color' ),
																																				 'wcfmvm_field_table_bg_color' => array( 'label' => __( 'Table Background Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_bg_color_settings', 'default' => '#ffffff', 'element' => '#wcfm-main-contentainer .wcfm_membership_element:nth-child(odd)', 'style' => 'background-color' ),
																																				 'wcfmvm_field_table_bg_heighlighter_color' => array( 'label' => __( 'Table Cell Heighlighter Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_bg_heighlighter_color_settings', 'default' => '#afd2cf', 'element' => '#wcfm-main-contentainer .wcfm_membership_element:nth-child(even), #wcfm-main-contentainer .wcfm_membership_box_wrraper .wcfm_membership_box_head .wcfm_membership_featured_top', 'style' => 'background-color' ),
																																				 'wcfmvm_field_table_head_bg_color' => array( 'label' => __( 'Table Head Background Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_head_bg_color_settings', 'default' => '#2d4e4c', 'element' => '#wcfm-main-contentainer .wcfm_membership_box_head', 'style' => 'background-color' ),
																																				 'wcfmvm_field_table_head_title_color' => array( 'label' => __( 'Membership Title Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_head_title_color_settings', 'default' => '#ececec', 'element' => '#wcfm-main-contentainer .wcfm_membership_box_head .wcfm_membership_title, #wcfm-main-contentainer .wcfm_membership_review_plan .wcfm_review_plan_title', 'style' => 'color' ),
																																				 'wcfmvm_field_table_head_price_color' => array( 'label' => __( 'Membership Price Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_head_price_color_settings', 'default' => '#ececec', 'element' => '#wcfm-main-contentainer .wcfm_membership_box_head .wcfm_membership_price .amount', 'style' => 'color', 'element2' => '#wcfm-main-contentainer .wcfm_membership_box_head .wcfm_membership_price .amount', 'style2' => 'border-color' ),
																																				 'wcfmvm_field_table_head_price_desc_color' => array( 'label' => __( 'Membership Price Description Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_head_price_desc_color_settings', 'default' => '#111111', 'element' => '#wcfm-main-contentainer .wcfm_membership_box_head .wcfm_membership_price_description', 'style' => 'color' ),
																																				 'wcfmvm_field_table_head_description_color' => array( 'label' => __( 'Membership Description Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_head_description_color_settings', 'default' => '#C3D9FF', 'element' => '#wcfm-main-contentainer .wcfm_membership_box_head .wcfm_membership_description, #wcfm-main-contentainer .wcfm_membership_review_plan .wcfm_review_plan_description', 'style' => 'color' ),
																																				 'wcfmvm_field_button_color' => array( 'label' => __( 'Subscribe Button Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_button_bg_color_settings', 'default' => '#2a3344', 'element' => '#wcfm_membership_container input.wcfm_submit_button, #wcfm-main-contentainer .wcfm_membership_box_head .wcfm_membership_title', 'style' => 'background-color' ),
																																				 'wcfmvm_field_button_hover_color' => array( 'label' => __( 'Subscribe Button Hover Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_button_bg_hover_color_settings', 'default' => '#00897b', 'element' => '#wcfm_membership_container input.wcfm_submit_button:hover', 'style' => 'background-color' ),
																																				 'wcfmvm_field_button_bg_color' => array( 'label' => __( 'Subscribe Button Text Color', 'wc-multivendor-membership' ), 'name' => 'wcfmvm_membership_table_button_color_settings', 'default' => '#ffffff', 'element' => '#wcfm_membership_container input.wcfm_submit_button', 'style' => 'color' ),
																																			) );
		
		return $color_options;
	}
	
	/**
	 * Create WCFMvm custom CSS
	 */
	function wcfmvm_create_membership_css() {
		global $WCFM, $WCFMvm;
		
		$wcfm_membership_options = get_option('wcfm_membership_options');
		$wcfmvm_color_options = array();
		if( isset( $wcfm_membership_options['membership_color_settings'] ) ) $wcfmvm_color_options = $wcfm_membership_options['membership_color_settings'];
		$color_options = $WCFMvm->wcfmvm_membership_color_setting_options();
		$custom_color_data = '';
		foreach( $color_options as $color_option_key => $color_option ) {
		  $custom_color_data .= $color_option['element'] . '{ ' . "\n";
			$custom_color_data .= "\t" . $color_option['style'] . ': ';
			if( isset( $wcfmvm_color_options[ $color_option['name'] ] ) ) { $custom_color_data .= $wcfmvm_color_options[ $color_option['name'] ]; } else { $custom_color_data .= $color_option['default']; }
			$custom_color_data .= ';' . "\n";
			$custom_color_data .= '}' . "\n\n";
			
			if( isset( $color_option['element2'] ) && isset( $color_option['style2'] ) ) {
				$custom_color_data .= $color_option['element2'] . '{ ' . "\n";
				$custom_color_data .= "\t" . $color_option['style2'] . ': ';
				if( isset( $wcfmvm_color_options[ $color_option['name'] ] ) ) { $custom_color_data .= $wcfmvm_color_options[ $color_option['name'] ]; } else { $custom_color_data .= $color_option['default']; }
				$custom_color_data .= ';' . "\n";
				$custom_color_data .= '}' . "\n\n";
			}
			
			if( isset( $color_option['element3'] ) && isset( $color_option['style3'] ) ) {
				$custom_color_data .= $color_option['element3'] . '{ ' . "\n";
				$custom_color_data .= "\t" . $color_option['style3'] . ': ';
				if( isset( $wcfmvm_color_options[ $color_option['name'] ] ) ) { $custom_color_data .= $wcfmvm_color_options[ $color_option['name'] ]; } else { $custom_color_data .= $color_option['default']; }
				$custom_color_data .= ';' . "\n";
				$custom_color_data .= '}' . "\n\n";
			}
			
			if( isset( $color_option['element4'] ) && isset( $color_option['style4'] ) ) {
				$custom_color_data .= $color_option['element4'] . '{ ' . "\n";
				$custom_color_data .= "\t" . $color_option['style4'] . ': ';
				if( isset( $wcfmvm_color_options[ $color_option['name'] ] ) ) { $custom_color_data .= $wcfmvm_color_options[ $color_option['name'] ]; } else { $custom_color_data .= $color_option['default']; }
				$custom_color_data .= ';' . "\n";
				$custom_color_data .= '}' . "\n\n";
			}
			
			if( isset( $color_option['element5'] ) && isset( $color_option['style5'] ) ) {
				$custom_color_data .= $color_option['element5'] . '{ ' . "\n";
				$custom_color_data .= "\t" . $color_option['style5'] . ': ';
				if( isset( $wcfmvm_color_options[ $color_option['name'] ] ) ) { $custom_color_data .= $wcfmvm_color_options[ $color_option['name'] ]; } else { $custom_color_data .= $color_option['default']; }
				$custom_color_data .= ';' . "\n";
				$custom_color_data .= '}' . "\n\n";
			}
		}
		
		$upload_dir      = wp_upload_dir();

		$files = array(
			array(
				'base' 		=> $upload_dir['basedir'] . '/wcfm',
				'file' 		=> 'wcfmvm-style-custom-' . time() . '.css',
				'content' 	=> $custom_color_data,
			)
		);

		$wcfmvm_style_custom = get_option( 'wcfmvm_style_custom' );
		if( file_exists( trailingslashit( $upload_dir['basedir'] ) . 'wcfm/' . $wcfmvm_style_custom ) ) {
			unlink( trailingslashit( $upload_dir['basedir'] ) . 'wcfm/' . $wcfmvm_style_custom );
		}
		
		foreach ( $files as $file ) {
			if ( wp_mkdir_p( $file['base'] ) ) {
				if ( $file_handle = @fopen( trailingslashit( $file['base'] ) . $file['file'], 'w' ) ) {
					$wcfmvm_style_custom = $file['file'];
					update_option( 'wcfmvm_style_custom', $file['file'] );
					fwrite( $file_handle, $file['content'] );
					fclose( $file_handle );
				}
			}
		}
		return $wcfmvm_style_custom;
	}
	
	/**
	 * Payment Gateway IPN listener 
	 */
  public function wcfmvm_ipn_listener() {
  	global $WCFM;
  	
		// Listen and handle PayPal IPN
		$wcfmvm_process_ipn = filter_input( INPUT_GET, 'wcfmvm_process_ipn' );
		if( $wcfmvm_process_ipn ) {
			wcfmvm_create_log( "WCfMvm IPN Received -->" );
			if ( $wcfmvm_process_ipn == 'paypal_ipn' ) {
				wcfmvm_create_log( "PayPal IPN Process Start -->" );
				include( $this->plugin_path . 'ipn/wcfmvm-handle-pp-ipn.php' );
				exit;
			}

			// Listen and handle Stripe Buy Now IPN
			if ( $wcfmvm_process_ipn == 'stripe_ipn' ) {
				wcfmvm_create_log( "Stripe IPN Process Start -->" );
				include( $this->plugin_path . 'ipn/wcfmvm-handle-stripe-ipn.php' );
				exit;
			}
	
			// Listen and handle Stripe Subscription IPN
			if ( $wcfmvm_process_ipn == 'stripe_subs_ipn' ) {
					include( $this->plugin_path . 'ipn/wcfmvm-handle-stripe-subs-ipn.php' );
					exit;
			}
	
			// Listen and handle Braintree Buy Now IPN
			/*$swpm_process_braintree_buy_now = filter_input(INPUT_GET, 'swpm_process_braintree_buy_now');
			if ($swpm_process_braintree_buy_now == '1') {
					include(SIMPLE_WP_MEMBERSHIP_PATH . 'ipn/swpm-braintree-buy-now-ipn.php');
					exit;
			}*/
		}
	}
	
	function register_vendor( $member_id ) {
		global $WCFM, $WCFMvm, $wpdb;
		
		$has_error = false;
		$wcfm_membership = get_user_meta( $member_id, 'temp_wcfm_membership', true );
		$shop_name = get_user_meta( $member_id, 'store_name', true );
			
		if( $wcfm_membership ) {
			$member_user = new WP_User(absint($member_id));
			$wcfm_membership = absint( $wcfm_membership );
			
			// Member static field data - 1.0.6
			$wcfmvm_registration_static_fields = get_option( 'wcfmvm_registration_static_fields', array() );
			$wcfmvm_static_infos = (array) get_user_meta( $member_id, 'wcfmvm_static_infos', true );
			
			// Membership Commission Data
			$commission = (array) get_post_meta( $wcfm_membership, 'commission', true );
			$commission_type = isset( $commission['type'] ) ? $commission['type'] : 'percent';
			$commission_value = isset( $commission['value'] ) ? $commission['value'] : '';
				
			$is_marketplace = wcfm_is_marketplace();
			
			if( $is_marketplace == 'wcmarketplace' ) {
				// Update user role
				$member_user->set_role('dc_vendor');
				
				// Creating Vendor Store
				$vendor = get_wcmp_vendor( $member_id );
				if ( $vendor ) {
					$vendor->generate_term();
					$vendor->generate_shipping_class();
					$vendor->update_page_title( wc_clean( $shop_name ) );
					
					// Set Vendor Store
					update_user_meta( $member_id, '_vendor_page_title', $shop_name );
					
					// Set Vendor Static field data
					if( !empty( $wcfmvm_registration_static_fields ) && !empty( $wcfmvm_static_infos ) ) {
						foreach( $wcfmvm_registration_static_fields as $wcfmvm_registration_static_field => $wcfmvm_registration_static_field_val ) {
							$field_value = array();
							$field_name = 'wcfmvm_static_infos[' . $wcfmvm_registration_static_field . ']';
							
							if( !empty( $wcfmvm_static_infos ) ) {
								$field_value = isset( $wcfmvm_static_infos[$wcfmvm_registration_static_field] ) ? $wcfmvm_static_infos[$wcfmvm_registration_static_field] : array();
							}
							
							switch( $wcfmvm_registration_static_field ) {
								case 'address':
									if( isset($field_value['addr_1']) ) {
										$store_address_fields = array( 	
																					'_vendor_address_1'  => 'addr_1',
																					'_vendor_address_2'  => 'addr_2',
																					'_vendor_country'    => 'country',
																					'_vendor_city'       => 'city',
																					'_vendor_state'      => 'state',
																					'_vendor_postcode'   => 'zip',
																			  );
		
										foreach( $store_address_fields as $store_address_field_key => $store_address_field ) {
											if( isset( $field_value[$store_address_field] ) ) {
												update_user_meta( $member_id, $store_address_field_key, $field_value[$store_address_field] );
											}
										}
									}
								break;
								
								case 'phone':
									update_user_meta( $member_id, '_vendor_phone', $field_value );
								break;
								
								default:
									do_action( 'wcfmvm_registration_static_field_data_store', $member_id, $wcfmvm_registration_static_field, $field_value );
								break;
							}
						}
					}
					
					// Set Vendor commission
					if( $commission_type && $commission_value ) {
						if( $commission_type == 'percent' ) {
							update_user_meta( $member_id, '_vendor_commission_percentage', $commission_value );
						} else {
							update_user_meta( $member_id, '_vendor_commission', $commission_value );
						}
					}
				} else {
					$has_error = true;
				}
			} elseif( $is_marketplace == 'wcpvendors' ) {
				if( !WC_Product_Vendors_Utils::is_vendor( $member_id ) ) {
					// Creating Vendor Store
					$term = wp_insert_term( $shop_name, WC_PRODUCT_VENDORS_TAXONOMY );
					if ( ! is_wp_error( $term ) ) {
						// Set Vendor commission
						$vendor_data = array();
						$vendor_data['admins']               = $member_id;
						$vendor_data['email']                = $member_user->user_email;
						$vendor_data['per_product_shipping'] = 'yes';
						$vendor_data['commission_type']      = ( $commission_type == 'percent' ) ? 'percentage' : 'fixed';
						$vendor_data['commission']           = $commission_value;
						
						update_term_meta( $term['term_id'], 'vendor_data', $vendor_data );
						
						// Update user role
						$member_user->set_role('wc_product_vendors_admin_vendor');
						update_user_meta( $member_id, '_wcpv_active_vendor', $term['term_id'] );
					} else {
						$has_error = true;
					}
				}
			} elseif( $is_marketplace == 'wcvendors' ) {
				// Update user role
				$member_user->set_role('vendor');
				
				// Set Vendor Store
				update_user_meta( $member_id, 'pv_shop_name', $shop_name );
				update_user_meta( $member_id, 'pv_shop_slug', sanitize_title( $shop_name ) );
				
				// Set Vendor Static field data
				if( !empty( $wcfmvm_registration_static_fields ) && !empty( $wcfmvm_static_infos ) ) {
					foreach( $wcfmvm_registration_static_fields as $wcfmvm_registration_static_field => $wcfmvm_registration_static_field_val ) {
						$field_value = array();
						$field_name = 'wcfmvm_static_infos[' . $wcfmvm_registration_static_field . ']';
						
						if( !empty( $wcfmvm_static_infos ) ) {
							$field_value = isset( $wcfmvm_static_infos[$wcfmvm_registration_static_field] ) ? $wcfmvm_static_infos[$wcfmvm_registration_static_field] : array();
						}
						
						switch( $wcfmvm_registration_static_field ) {
							case 'address':
								if( isset($field_value['addr_1']) ) {
									$store_address_fields = array( 	
																				'_wcv_store_address1'   => 'addr_1',
																				'_wcv_store_address2'   => 'addr_2',
																				'_wcv_store_country'    => 'country',
																				'_wcv_store_city'       => 'city',
																				'_wcv_store_state'      => 'state',
																				'_wcv_store_postcode'   => 'zip',
																			);
	
									foreach( $store_address_fields as $store_address_field_key => $store_address_field ) {
										if( isset( $field_value[$store_address_field] ) ) {
											update_user_meta( $member_id, $store_address_field_key, $field_value[$store_address_field] );
										}
									}
								}
							break;
							
							case 'phone':
								update_user_meta( $member_id, '_wcv_store_phone', $field_value );
							break;
							
							default:
								do_action( 'wcfmvm_registration_static_field_data_store', $member_id, $wcfmvm_registration_static_field, $field_value );
							break;
						}
					}
				}
				
				// Set Vendor commission
				if( $commission_type && $commission_value ) {
					update_user_meta( $member_id, 'pv_custom_commission_rate', $commission_value );
					if( $commission_type == 'percent' ) {
						update_user_meta( $member_id, '_wcv_commission_type', 'percent' );
						update_user_meta( $member_id, '_wcv_commission_percent', $commission_value );
					} else {
						update_user_meta( $member_id, '_wcv_commission_type', 'fixed' );
						update_user_meta( $member_id, '_wcv_commission_amount', $commission_value );
					}
				}
			} elseif( $is_marketplace == 'dokan' ) {
				// Update user role
				$member_user->set_role('seller');
				
				$dokan_settings = array(
						'store_name'     => strip_tags( $shop_name ),
						'social'         => array(),
						'payment'        => array(),
						'phone'          => '',
						'show_email'     => 'no',
						'location'       => '',
						'find_address'   => '',
						'dokan_category' => '',
						'banner'         => 0,
				);
		
				update_user_meta( $member_id, 'dokan_profile_settings', $dokan_settings );
				
				// Set Vendor Store
				update_user_meta( $member_id, 'dokan_store_name', $shop_name );
				
				// Set Vendor Static field data
				if( !empty( $wcfmvm_registration_static_fields ) && !empty( $wcfmvm_static_infos ) ) {
					foreach( $wcfmvm_registration_static_fields as $wcfmvm_registration_static_field => $wcfmvm_registration_static_field_val ) {
						$field_value = array();
						$field_name = 'wcfmvm_static_infos[' . $wcfmvm_registration_static_field . ']';
						
						if( !empty( $wcfmvm_static_infos ) ) {
							$field_value = isset( $wcfmvm_static_infos[$wcfmvm_registration_static_field] ) ? $wcfmvm_static_infos[$wcfmvm_registration_static_field] : array();
						}
						
						switch( $wcfmvm_registration_static_field ) {
							case 'address':
								if( isset($field_value['addr_1']) ) {
									$dokan_settings['address'] = array();
									$store_address_fields = array( 	
																				'street_1'   => 'addr_1',
																				'street_2'   => 'addr_2',
																				'country'    => 'country',
																				'city'       => 'city',
																				'state'      => 'state',
																				'zip'        => 'zip',
																			);
	
									foreach( $store_address_fields as $store_address_field_key => $store_address_field ) {
										if( isset( $field_value[$store_address_field] ) ) {
											$dokan_settings['address'][$store_address_field_key] = $field_value[$store_address_field];
										}
									}
								}
							break;
							
							case 'phone':
								$dokan_settings['phone'] = $field_value;
							break;
							
							default:
								do_action( 'wcfmvm_registration_static_field_data_store', $member_id, $wcfmvm_registration_static_field, $field_value );
							break;
						}
					}
					update_user_meta( $member_id, 'dokan_profile_settings', $dokan_settings );
					update_user_meta( $member_id, 'can_post_product', '1' );
				}
				
				// Set Vendor commission
				if( $commission_type && $commission_value ) {
					if( $commission_type == 'percent' ) {
						update_user_meta( $member_id, 'dokan_admin_percentage_type', 'percentage' );
						update_user_meta( $member_id, 'dokan_admin_percentage', $commission_value );
					} else {
						update_user_meta( $member_id, 'dokan_admin_percentage_type', 'flat' );
						update_user_meta( $member_id, 'dokan_admin_percentage', $commission_value );
					}
				}
				
				do_action( 'dokan_new_seller_created', $member_id, $dokan_settings );
			}
			
			// Check and release from old membership
			$vendor_old_membership = get_user_meta( $member_id, 'wcfm_membership', true );
			if( $vendor_old_membership ) {
				// Old Membership list update
				$old_membership_users = (array) get_post_meta( $vendor_old_membership, 'membership_users', true );
				if( !empty( $old_membership_users ) ) {
					if( ( $key = array_search( $member_id, $old_membership_users ) ) !== false ) {
						unset( $old_membership_users[$key] );
					}
					update_post_meta( $vendor_old_membership, 'membership_users', $old_membership_users );
				}
				
				// Old Group vendor list update
				$old_associated_group = get_post_meta( $vendor_old_membership, 'associated_group', true );
				if( $old_associated_group ) {
					$old_group_vendors = (array) get_post_meta( $old_associated_group, '_group_vendors', true );
					if( !empty( $old_group_vendors ) ) {
						if( ( $key = array_search( $member_id, $old_group_vendors ) ) !== false ) {
							unset( $old_group_vendors[$key] );
						}
						update_post_meta( $old_associated_group, '_group_vendors', $old_group_vendors );
					}
				}
			}
			
			// User membership set
			update_user_meta( $member_id, 'wcfm_membership', $wcfm_membership );
				
			// Membership user update
			$membership_users = (array) get_post_meta( $wcfm_membership, 'membership_users', true );
			$membership_users[] = $member_id;
			update_post_meta( $wcfm_membership, 'membership_users', $membership_users );
				
			// Group user update
			if( WCFM_Dependencies::wcfmgs_plugin_active_check() ) {
				$associated_group = get_post_meta( $wcfm_membership, 'associated_group', true );
				if( $associated_group ) {
					$associated_group = absint( $associated_group );
					$group_vendors = (array) get_post_meta( $associated_group, '_group_vendors', true );
					$group_vendors[] = $member_id;
					update_post_meta( $associated_group, '_group_vendors', $group_vendors );
					
					// User Group update
					$wcfm_vendor_groups = array( $associated_group );
					update_user_meta( $member_id, '_wcfm_vendor_group', $wcfm_vendor_groups  );
				}
			}
			
			do_action( 'wcfm_membership_subscription_complete', $member_id );
				
			if( !$has_error ) {
				define( 'DOING_WCFM_EMAIL', true );
				
				// Sending Mail to new subscriber
				$subscription_welcome_email_subject = get_option( 'wcfm_membership_subscription_welcome_email_subject', '{site_name}: Successfully Subscribed' );
				$subscription_welcome_email_content = get_option( 'wcfm_membership_subscription_welcome_email_content', '' );
				if( !$subscription_welcome_email_content ) {
					$subscription_welcome_email_content = "Dear {first_name},
																<br /><br />
																You have successfully subscribed to our membership plan. 
																<br /><br />
																Your account already setup and ready to configure.
																<br /><br />
																Kindly follow the below the link to visit your dashboard.
																<br /><br />
																Dashboard: {dashboard_url} 
																<br /><br />
																Thank You";
				}
																 
				$subject = str_replace( '{site_name}', get_bloginfo( 'name' ), $subscription_welcome_email_subject );
				$message = str_replace( '{dashboard_url}', get_wcfm_url(), $subscription_welcome_email_content );
				$message = str_replace( '{first_name}', $member_user->first_name, $message );
				$message = apply_filters( 'wcfm_email_content_wrapper', $message, __( 'Welcome to the store!', 'wc-multivendor-membership' ) );
				
				wp_mail( $member_user->user_email, $subject, $message );
				
				// Sending Mail to Admin
				$subscription_admin_notication_subject = get_option( 'wcfm_membership_subscription_admin_notication_subject', '{site_name}: A new vendor registered' );
				$subscription_admin_notication_content = get_option( 'wcfm_membership_subscription_admin_notication_content', '' );
				if( !$subscription_admin_notication_content ) {
					$subscription_admin_notication_content = "Dear Admin,
																										<br /><br />
																										A new vendor <b>{vendor_name}</b> (Store: <b>{vendor_store}</b>) successfully subscribed to our membership plan <b>{membership_plan}</b>. 
																										<br /><br />
																										Kindly follow the below the link to more details.
																										<br /><br />
																										Dashboard: {vendor_url} 
																										<br /><br />
																										Thank You";
				}
																 
				$subject = str_replace( '{site_name}', get_bloginfo( 'name' ), $subscription_admin_notication_subject );
				$message = str_replace( '{dashboard_url}', get_wcfm_url(), $subscription_admin_notication_content );
				$message = str_replace( '{vendor_name}', $member_user->first_name, $message );
				$message = str_replace( '{vendor_store}', $shop_name, $message );
				$message = str_replace( '{membership_plan}', get_the_title( $wcfm_membership ), $message );
				$message = str_replace( '{vendor_url}', get_wcfm_vendors_manage_url($member_id), $message );
				$message = apply_filters( 'wcfm_email_content_wrapper', $message, __( 'New Vendor', 'wc-multivendor-membership' ) );
				
				wp_mail( get_bloginfo( 'admin_email' ), $subject, $message ); 
				
				// Admin Desktop Notification
				$author_id = $member_id;
				$author_is_admin = 0;           
				$author_is_vendor = 1;
				$message_to = 0;
				$is_notice = 0;
				$is_direct_message = 1;
				$wcfm_messages_type = 'membership';
				
				$wcfm_messages = sprintf( __( '<b>%s</b> (Store: <b>%s</b>) subscribed for <b>%s</b> membership plan.', 'wc-multivendor-membership' ), '<a href="' . get_wcfm_vendors_manage_url($member_id) . '" target="_blank">' . $member_user->first_name . '</a>', $shop_name, get_the_title( $wcfm_membership ) );
				$wcfm_create_message     = "INSERT into {$wpdb->prefix}wcfm_messages 
																		(`message`, `author_id`, `author_is_admin`, `author_is_vendor`, `is_notice`, `is_direct_message`, `message_to`, `message_type`)
																		VALUES
																		('{$wcfm_messages}', {$author_id}, {$author_is_admin}, {$author_is_vendor}, {$is_notice}, {$is_direct_message}, {$message_to}, '{$wcfm_messages_type}')";
																
				$wpdb->query($wcfm_create_message);
				
				$messageid = $wpdb->insert_id;
				$todate = date('Y-m-d H:i:s');
				if( $messageid ) {
					$wcfm_read_message     = "INSERT into {$wpdb->prefix}wcfm_messages_modifier 
																			(`message`, `is_read`, `read_by`, `read_on`)
																			VALUES
																			({$messageid}, 1, {$author_id}, '{$todate}')";
					$wpdb->query($wcfm_read_message);
				}
							
				delete_user_meta( $member_id, 'temp_wcfm_membership' );
			}
		}
		return $has_error;
	}
	
	function store_subscription_data( $member_id, $paymode, $transaction_id, $transaction_type, $transaction_status, $transaction_details ) {
		global $WCFM, $WCFMvm, $wpdb;
		
		$has_error = false;
		$member_id = absint( $member_id );
		$membership_id = get_user_meta( $member_id, 'wcfm_membership', true );
			
		if( $membership_id ) {
			$member_user = new WP_User( $member_id );
			$membership_id = absint( $membership_id );
			
			$subscription = (array) get_post_meta( $membership_id, 'subscription', true );
			$is_free = isset( $subscription['is_free'] ) ? 'yes' : 'no';
			$period_options = array( 'D' => __( 'Day(s)', 'wc-multivendor-membership' ), 'M' => __( 'Month(s)', 'wc-multivendor-membership' ), 'Y' => __( 'Year(s)', 'wc-multivendor-membership' ) );
			$subscription_type = '';
			$subscription_amt = 0;
			$subscription_interval = '';
			
			// Update quick info in User Profile
			if( $transaction_status != 'Cancelled' ) {
				update_user_meta( $member_id, 'wcfm_transaction_id', $transaction_id );
				update_user_meta( $member_id, 'wcfm_subscription_status', 'active' );
				update_user_meta( $member_id, 'wcfm_membership_paymode', $paymode );
			} else {
				update_user_meta( $member_id, 'wcfm_subscription_status', 'cancelled' );
			}
				
			if( $is_free == 'yes' ) {
				$subscription_type = 'free';
				update_user_meta( $member_id, 'wcfm_subscription_status', 'active' );
			} else {
				$subscription_type = isset( $subscription['subscription_type'] ) ? $subscription['subscription_type'] : 'one_time';
				
				if( ( $paymode == 'paypal' || $paymode == 'stripe' || $paymode == 'bank_transfer' ) && ( $subscription_type == 'one_time' ) ) {
					$subscription_amt = isset( $subscription['one_time_amt'] ) ? floatval($subscription['one_time_amt']) : '1';
					$subscription_interval = 0;
				} elseif( ( $paymode == 'paypal' || $paymode == 'stripe' || $paymode == 'bank_transfer' ) && ( $subscription_type == 'recurring' ) ) {
					$subscription_amt = isset( $subscription['trial_amt'] ) ? $subscription['trial_amt'] : '';
					$subscription_interval = isset( $subscription['trial_period'] ) ? $subscription['trial_period'] : '';
					$subscription_interval .= ' ' . $period_options[$subscription['trial_period_type']];
				} elseif( ( $paymode == 'paypal_subs' || $paymode == 'stripe_subs' || $paymode == 'bank_transfer_subs' ) && ( $subscription_type == 'recurring' ) ) {
					$subscription_amt = isset( $subscription['billing_amt'] ) ? floatval($subscription['billing_amt']) : '1';
					$subscription_interval = isset( $subscription['billing_period'] ) ? $subscription['billing_period'] : '';
					$subscription_interval .= ' ' . $period_options[$subscription['billing_period_type']];
					
					update_user_meta( $member_id, 'wcfm_subscription_profile_id', $transaction_id );
					if( $transaction_status != 'Completed' ) {
						update_user_meta( $member_id, 'wcfm_subscription_status', 'blocked' );
					}
				}
			}
			$subscription_amt = absint($subscription_amt);
			
			// Update quick info in User Profile
			update_user_meta( $member_id, 'wcfm_subscription_type', $subscription_type );
			
			// Membership Subscription Query
			$wcfm_membership_subscription = "INSERT into {$wpdb->prefix}wcfm_membership_subscription 
																			(`vendor_id`, `membership_id`, `subscription_type`, `subscription_amt`, `subscription_interval`, `event`, `pay_mode`, `transaction_id`, `transaction_type`, `transaction_status`, `transaction_details`)
																			VALUES
																			( $member_id, $membership_id, '{$subscription_type}', {$subscription_amt}, '{$subscription_interval}', '{$transaction_type}', '{$paymode}', '{$transaction_id}', '{$transaction_type}', '{$transaction_status}', '{$transaction_details}')
																			ON DUPLICATE KEY UPDATE
																			`subscription_type`     = '{$subscription_type}',
																			`subscription_amt`      =  {$subscription_amt},
																			`subscription_interval` = '{$subscription_interval}',
																			`event`                 = '{$transaction_type}',
																			`pay_mode`              = '{$paymode}',
																			`transaction_status`    = '{$transaction_status}',
																			`transaction_details`   = '{$transaction_details}'
																			";
			$wpdb->query($wcfm_membership_subscription);
		}
	}
	
	/**
	 * WC Checkout membership purchase registration process on Order complete
	 */
	function wcfmvm_registration_process_on_order_completed( $order_id ) {
		global $WCFM, $WCFMvm, $wpdb;
		$wcfm_subcription_products = get_option( 'wcfm_subcription_products', array() );
		
		if( !empty( $wcfm_subcription_products ) ) {
			$order         = new WC_Order( $order_id );
			$line_items    = $order->get_items( apply_filters( 'woocommerce_admin_order_item_types', 'line_item' ) );
			foreach ( $line_items as $item_id => $item ) {
				if( in_array( $item->get_product_id(), $wcfm_subcription_products ) ) {
					$member_id       = absint( $order->get_user_id() );
					$member_user     = new WP_User( absint( $member_id ) );
					$wcfm_membership = get_user_meta( $member_id, 'temp_wcfm_membership', true );
					$shop_name       = get_user_meta( $member_id, 'store_name', true );
					$paymode         = $order->get_payment_method();
					
					if( $wcfm_membership ) {
						update_user_meta( $member_id, 'wcfm_membership_paymode', $paymode );
						update_user_meta( $member_id, 'wcfm_membership_order', $order_id );
						$required_approval = get_post_meta( $wcfm_membership, 'required_approval', true ) ? get_post_meta( $wcfm_membership, 'required_approval', true ) : 'no';
						
						if( $required_approval == 'no' ) {
							$has_error = $WCFMvm->register_vendor( $member_id );
							$WCFMvm->store_subscription_data( $member_id, $paymode, '', $paymode.'_subscription', 'Completed', '' );
						} else {
							define( 'DOING_WCFM_EMAIL', true );
							
							// Vendor Approval Admin Email Notification
							$onapproval_admin_notication_subject = get_option( 'wcfm_membership_onapproval_admin_notication_subject', '{site_name}: A vendor application waiting for approval' );
							$onapproval_admin_notication_content = get_option( 'wcfm_membership_onapproval_admin_notication_content', '' );
							if( !$onapproval_admin_notication_content ) {
								$onapproval_admin_notication_content = "Dear Admin,
																												<br /><br />
																												A new vendor <b>{vendor_name}</b> (Store: <b>{vendor_store}</b>) has just applied to our membership plan <b>{membership_plan}</b>. 
																												<br /><br />
																												Kindly follow the below the link to approve/reject the application.
																												<br /><br />
																												Application: {notification_url} 
																												<br /><br />
																												Thank You";
							}
																			 
							$subject = str_replace( '{site_name}', get_bloginfo( 'name' ), $onapproval_admin_notication_subject );
							$message = str_replace( '{dashboard_url}', get_wcfm_url(), $onapproval_admin_notication_content );
							$message = str_replace( '{vendor_name}', $member_user->first_name, $message );
							$message = str_replace( '{vendor_store}', $shop_name, $message );
							$message = str_replace( '{membership_plan}', get_the_title( $wcfm_membership ), $message );
							$message = str_replace( '{notification_url}', get_wcfm_messages_url(), $message );
							$message = apply_filters( 'wcfm_email_content_wrapper', $message, __( 'Vendor Approval', 'wc-multivendor-membership' ) );
							
							wp_mail( get_bloginfo( 'admin_email' ), $subject, $message ); 
							
							// Vendor Approval Admin Desktop Notification
							$author_id = $member_id;
							$author_is_admin = 0;
							$author_is_vendor = 1;
							$message_to = 0;
							$is_notice = 0;
							$is_direct_message = 1;
							$wcfm_messages_type = 'vendor_approval';
							$wcfm_messages = sprintf( __( '<b>%s</b> (Store: <b>%s</b>) subscription for <b>%s</b> plan waiting for approval.', 'wc-multivendor-membership' ), $member_user->first_name, $shop_name, get_the_title( $wcfm_membership ) );
							$wcfm_create_message     = "INSERT into {$wpdb->prefix}wcfm_messages 
																			(`message`, `author_id`, `author_is_admin`, `author_is_vendor`, `is_notice`, `is_direct_message`, `message_to`, `message_type`)
																			VALUES
																			('{$wcfm_messages}', {$author_id}, {$author_is_admin}, {$author_is_vendor}, {$is_notice}, {$is_direct_message}, {$message_to}, '{$wcfm_messages_type}')";
																
							$wpdb->query($wcfm_create_message);
							
							update_user_meta( $member_id, 'wcfm_subscription_status', 'pending' );
						}
					}
					break;
				}
			}
		}
	}
	
	/**
	 * ON WP delete user clean WCFM membership
	 */
	function wcfmvm_delete_user( $member_id ) {
		global $WCFM, $WCFMvm;
		
		$vendor_old_membership = get_user_meta( $member_id, 'wcfm_membership', true );
		if( $vendor_old_membership ) {
			// Membership list update
			$old_membership_users = (array) get_post_meta( $vendor_old_membership, 'membership_users', true );
			if( !empty( $old_membership_users ) ) {
				if( ( $key = array_search( $member_id, $old_membership_users ) ) !== false ) {
					unset( $old_membership_users[$key] );
				}
				update_post_meta( $vendor_old_membership, 'membership_users', $old_membership_users );
			}
			
			// Group vendor list update
			$old_associated_group = get_post_meta( $vendor_old_membership, 'associated_group', true );
			if( $old_associated_group ) {
				$old_group_vendors = (array) get_post_meta( $old_associated_group, '_group_vendors', true );
				if( !empty( $old_group_vendors ) ) {
					if( ( $key = array_search( $member_id, $old_group_vendors ) ) !== false ) {
						unset( $old_group_vendors[$key] );
					}
					update_post_meta( $old_associated_group, '_group_vendors', $old_group_vendors );
				}
			}
		}
		
		$paymode = get_user_meta( $member_id, 'wcfm_membership_paymode', true );
		$WCFMvm->store_subscription_data( $member_id, $paymode, '', 'subscr_cancel', 'Cancelled', 'Manual Cancellation' );
	}
}