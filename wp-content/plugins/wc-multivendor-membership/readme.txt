﻿=== WooCommerce Memberships for Multivendor Marketplace ===
Contributors: wclovers
Tags: woocommerce membership, subscription, members, multivendor marketplace, multi vendor 
Donate link: https://wclovers.com
Requires at least: 4.4
Tested up to: 4.9
WC requires at least: 3.0
WC tested up to: 3.2.0
Requires PHP: 5.6
Stable tag: 1.3.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A simple woocommerce memberships plugin for offering free and premium subscription for your multi-vendor marketplace - WC Marketplace, WC Vendors & Dokan.

== Description ==
A simple woocommerce memberships plugin for offering FREE AND PREMIUM SUBSCRIPTION for your multi-vendor marketplace (WC Marketplace, WC Vendors, WC Product Vendors & Dokan).

You may set up unlimited membership levels (example: free, silver, gold etc) with different pricing plan, capabilities and commission.

[youtube https://www.youtube.com/watch?v=CVRQnrm7nC0]

> Read full documentation here -
>
> [WCFM - Membership](https://wclovers.com/knowledgebase/wcfm-membership/)
> [Video Tutorials](https://wclovers.com/wcfm-tutorials/)

> Experience membership subscription here -
>
> [Membership Demo](http://wcvendors.wcfmdemos.com/vendor-membership/)

= HAVE SUPPORT OF MY MULTI-VENDOR =

WooCommerce Multivaendor Membership works with all popular marketplace add-ons -

* WC Marketplace
* WC Vendors and WC Vednors PRO
* Dokan Lite and Dokan PRO
* WooCommerce Product Vendors

= HAVE FREE AND PAID MEMBERSHIPS =

You can configure it to have free and/or paid memberships on your site. Paid membership payment is handled securely via PayPal.

Both one time and recurring/subscription payments are supported.

= HAVE SUPPORT OF MY PAYMENT GATEWAYS =

You can setup membsership subsctiptions with all flexibilities and with different intgegrated payment options as well - 

* FREE
* PayPal
* Bank Transfer
* Stripe

Are you missing your payment gateway then setup using WC Products -

[youtube https://youtu.be/SfOMIxNfr3w]

= HAVE DIFFERENT COMMISSION MODEL =

You can configure different commission structure for each membership level. Fixed and percent both are supported.

= HAVE DIFFERENT CAPABILITIES =

You can assign different capability module for each membership level.

You may create totally different capability group with all flexibilities (example: product limit, categories, product types etc) for each membership level.

But this is not directly part of this add-on, Capability modules are come from [WCFM - Groups & Staffs](http://wclovers.com/product/woocommerce-frontend-manager-groups-staffs/). 

= MEMBERSHIP DETAILS = 

Your vednors will have full details of their membership under their WCFM dashboard.

They may also change their subscription plan any time - upgrade or downgrade.

= WHAT ABOUT NON-VENDOR USERS = 

Any user of the site (except Administrator and Shop manager) may apply for membership subscription.

Just to mention, it works as a add-on for [WooCommerce Frontend Manager](https://wordpress.org/plugins/wc-frontend-manager).

= Translations =

- Potuguese (Thanks to Rafael Sartori)
- Spanish   (Thanks to @ffthernandez)
- German    (Thanks to Ciao)

= Feedback = 

All we want is love. We are extremely responsive about support requests - so if you face a problem or find any bugs, shoot us a mail or post it in the support forum, and we will respond within 6 hours(during business days). If you get the impulse to rate the plugin low because it is not working as it should, please do wait for our response because the root cause of the problem may be something else. 

It is extremely disheartening when trigger happy users downrate a plugin for no fault of the plugin. 

Feel free to reach us either via our [support forum](https://wclovers.com/forums) or [WordPress.org](https://wordpress.org/support/plugin/wc-multivendor-membership), happy to serve anything you looking for. 

Really proud to serve and enhance [WooCommerce](http://woocommerce.com).

Be with us ... Team [WC Lovers](https://wclovers.com)

== Installation ==

= Minimum Requirements =

* WordPress 4.7 or greater
* WooCommerce 3.0 or greater
* PHP version 5.6 or greater
* MySQL version 5.0 or greater

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't need to leave your web browser. To do an automatic install of WooCommerce Frontend Manager, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type "WooCommerce Multivendor Membership" and click Search Plugins. Once you've found our eCommerce plugin you can view details about it such as the point release, rating and description. Most importantly of course, you can install it by simply clicking "Install Now".

= Manual installation =

The manual installation method involves downloading our eCommerce plugin and uploading it to your webserver via your favourite FTP application. The WordPress codex contains [instructions on how to do this here](https://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).

== FAQ ==

NONE.

== Screenshots ==

1. Membership Plan Table
2. Membership Dashboard
3. Membership Subscription Types
4. Membership Recurring Subscription with Trial
5. New Membership
6. Membership Commission setup
7. Membership Capability Group
8. Membership Table Styling
9. Membership Thank You Content
10. Membership Payment Page
11. Membership Thank You Page
12. Membership Registration Page
13. Membership General Settings
14. Membership Feature List Set up
15. Membership Welcome Email
16. Membership Details - Cancel & Upgrade

== Changelog ==

= 1.3.2 =
* Feature   - Registration email verification option added
* Enhance   - WC 3.4.0 compatibility added
* Tweak     - Free and Cancel membership replaced by Basic Memebrship option
* Tweak     - Membership setting page re-arranged
* Fixed     - Membership table display CSS issues resolved

= 1.3.1 =
* Enhance   - WCfM 4.1.0 compatibility added
* Enhance   - WC Vendors 2.0 compatibility added

= 1.3.0 =
* Feature   - Membership purchase using WC Product / Checkout option added, now you may use any WC payment options. (https://youtu.be/SfOMIxNfr3w)

= 1.2.4 =
* Fixed   - Stripe recurring PHP warning issue resolved

= 1.2.3 =
* Fixed   - Stripe class redifined from other plugins issue resolved
* Fixed   - Plan price empty PHP warning issue resolved

= 1.2.2 =
* Feature - Stripe payment support added
* Enhance - Membership emails wrap with WC email template
* Enhance - Translations updated
* Fixed   - Membership table long price display issue resolved
* Fixed   - Free membership registration CSS incuded in all pages issue resolved
* Fixed   - Some spelling corrected

= 1.2.1 =
* Feature - Vendor store phone static field support added
* Feature - Membership subscribe button short code "[wcfmvm_subscribe id="*"]" support added
* Enhance - WCfM 4.0.0 compatibility added
* Fixed   - Membership plan change issue resolved
* Fixed   - On user delete membership delete issue resolved
* Fixed   - Membership registration state field NULL issue resolved
* Fixed   - Membership pay mode display issue resolved

= 1.2.0 =
* Feature - Free membership registration separate page added
* Feature - "wcfm_vendor_registration" short code added 
* Enhance - Membership conditional delete option added
* Enahnce - "wcfm_membership_price_display" filter added for membership price display modification
* Enhamce - Admin now allowed to view Memberships page

= 1.1.9 =
* Feature - Registration Terms & Condition check support added 
* Enhance - Free membership payment step change to "Confirmation"
* Enahnce - Vendor approval pop-up changed to colorbox

= 1.1.8 =
* Fixed - Translation loading issue resolved

= 1.1.7 =
* Feature - WCFM vendor custom badges compatiblity added
* Enhance - Spanish (Thanks to @ffthernandez) translation added

= 1.1.6 =
* Feature - Membership registration store address field support added
* Enhance - Portuguese(Brazil) translation added
* Fixed   - Membership subscription page WC block JS missing issue resolved

= 1.1.5 =
* Fixed   - Membership plan change old membership users list update issue resolved
* Fixed   - Membership plan change old group users list update issue resolved
* Fixed   - Email content save escape charatater issue resolved

= 1.1.4 =
* Feature - Membership Payment terms option added
* Fixed   - Membership table description display issue resolved

= 1.1.3 =
* Feature - Membership section added in vendor manage page
* Enhance - Registration additional info displayed at admin Vendor Details & vendor Profile page
* Fixed   - Free trial period issue resolved
* Fixed   - Without feature membership table display issue resolved
* Fixed   - Membership table description display issue resolved
* Fixed   - Too many membership option box display issue resolved

= 1.1.2 =
* Feature - Membership section added in vendor manage page
* Enhance - Registration additional info displayed at admin Vendor Details & vendor Profile page
* Fixed   - Without feature membership table display issue resolved
* Fixed   - Membership table description display issue resolved
* Fixed   - Too many membership option box display issue resolved

= 1.1.1 =
* Feature - Membership Subscription pending approval option added

= 1.0.4 =
* Feature - Membership plan change & upgrade option added
* Feature - New subscription Admin Email notification added
* Feature - Membership subscription cancel opton added
* Fixed   - Subsciption process steps responsive issue resolved

= 1.0.3 =
* Feature - Bank Transfer payment option added
* Fixed   - Membership table mobile view issue resolved
* Fixed   - Membership spell typo error fixed

= 1.0.2 =
* Enhance - Membership Plan table redefined

= 1.0.1 =
* Basic set up version release

= 1.0.0 =
* Initial version release

== Upgrade Notice ==

= 1.3.2 =
* Feature   - Registration email verification option added
* Enhance   - WC 3.4.0 compatibility added
* Tweak     - Free and Cancel membership replaced by Basic Memebrship option
* Tweak     - Membership setting page re-arranged
* Fixed     - Membership table display CSS issues resolved