<?php
/**
 * Template Name: Check User Login
 */

	if(is_user_logged_in()) {
		header("Location: http://www.cloudshippingservices.com/membership-payment");
	}
	else 
	{
		header("Location: http://www.cloudshippingservices.com/customer-membership-form");
	}
?>