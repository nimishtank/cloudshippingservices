<?php
/**
 * Template Name: Customer Membership Template
 */

get_header(); the_post(); ?>

<?php the_content() ?>

<div style="width:400px; padding:10px 10px 10px 10px; margin:0 auto; margin-top:50px; margin-bottom:50px;">
   <div style="text-align:center;">
      <h3>Customer Membership - Sign Up</h3>
   </div>
   <form method="post" action="https://www.cloudshippingservices.com/wp-content/themes/my-listing/customer-details.php">
      <div><input type="text" id="custname" name="custname" placeholder="Your Name"></div>
      <div><input type="text" id="custemail" name="custemail" placeholder="Your Email"></div>
      <div><input type="password" name="custpassword" placeholder="Please enter password"></div>
      <div><input type="hidden" name="form_type" value="Customer"></div>
      <div style="margin-top: 20px;"><input type="submit" name="submit" class="wpcf7-form-control wpcf7-submit"></div>
   </form>
</div>

<?php get_footer();
