<?php
/**
 * Template Name: Location Check User Login
 */

	if(is_user_logged_in()) {
		header("Location: https://www.cloudshippingservices.com/location-payment-monthly/");
	}
	else 
	{
		header("Location: https://www.cloudshippingservices.com/location-membership-form");
	}
?>